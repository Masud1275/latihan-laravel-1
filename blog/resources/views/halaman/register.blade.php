<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <p><h1>Buat Account Baru</h1></p>
    <p><h3>Sign Up Form</h3></p>

    <form action="/welcome" method="POST">
        @csrf
        <label>First Name:</label><br><br>
        <input type="text" name="nama_depan"><br><br>
        <label>Last Name:</label><br><br>
        <input type="text" name="nama_akhir"><br><br>

        <label>Gender:</label><br><br>
        <input type="radio" value="pria" name="jk">Male<br>
        <input type="radio" value="wanita" name="jk">Female<br>
        <input type="radio" value="lain" name="jk">Other<br><br>

        <label>Nationality:</label><br><br>
        <select name="wn">
            <option value="1">Indonesian</option>
            <option value="2">Foreigners</option>
        </select><br><br>
        
        <label>Language Spoken: </label><br><br>
        <input type="checkbox" name="bahasa">Bahasa Indonesia<br>
        <input type="checkbox" name="inggris">English<br>
        <input type="checkbox" name="lainnya">Other<br><br>

        <label>Bio:</label><br><br>
        <textarea name="biodata"  cols="30" rows="10"></textarea><br>

        <input type="submit" value="Sign Up">


    </form>

</body>
</html>